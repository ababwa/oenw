<?php
	include_once 'utils.php';
	
	function landing() {
		out(line('<p>Enter a word or non-word:</p>'));
		out(line(tabs(4) . '<form method="get">'));
		out(line(tabs(5) . '<input type="hidden" name="page" value="search"/>'));
		out(line(tabs(5) . '<input type="text" name="query" class="input_box"/>'));
		out(line(tabs(5) . '<br>'));
		out(line(tabs(5) . '<input type="submit" value="Search"/>'));
		out(line(tabs(4) . '</form>'));
	}
?>
