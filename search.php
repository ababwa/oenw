<?php
	include_once 'utils.php';
	
	function search($query) {
		$con = get_db_connection();
		
		$stmt = $con->prepare('call search(?)');
		$stmt->bind_param('s', $query);
		$stmt->execute();
		$stmt->bind_result($id, $nonword);
		
		out(line('Results for "' . $query . '"'));
		
		while($stmt->fetch()) {
			out(line(tabs(4) . '<div class="result"><a href="?page=nonword&id=' . $id . '">' . htmlspecialchars($nonword) . '</a></div>'));
		}
		
		$stmt->close();
		$con->close();
	}
?>
