<?php
	include_once 'utils.php';
	
	function row($label, $content) {
		out(line(tabs(5) . '<div class="row">'));
		out(line(tabs(6) . '<div class="col-md-3">'));
		out(line(tabs(7) . tag('p', $label)));
		out(line(tabs(6) . '</div>'));
		out(line(tabs(6) . '<div class="col-md-9">'));
		out($content);
		out(line(tabs(6) . '</div>'));
		out(line(tabs(5) . '</div>'));
	}
	
	function nonword($nonword_id) {
		$con = get_db_connection();
		
		$stmt_get_nonword = $con->prepare('select word, user_id, status_id, submit_time from nonword where id = ?');
		$stmt_get_nonword->bind_param('i', $nonword_id);
		$stmt_get_nonword->execute();
		$stmt_get_nonword->bind_result($word, $user_id, $status_id, $submit_time);
		$stmt_get_nonword->fetch();
		$stmt_get_nonword->close();
		
		$stmt_get_user = $con->prepare('select name from user where id = ?');
		$stmt_get_user->bind_param('i', $user_id);
		$stmt_get_user->execute();
		$stmt_get_user->bind_result($user_name);
		$stmt_get_user->fetch();
		$stmt_get_user->close();
		
		$stmt_get_nonword_status = $con->prepare('select name from nonword_status where id = ?');
		$stmt_get_nonword_status->bind_param('i', $status_id);
		$stmt_get_nonword_status->execute();
		$stmt_get_nonword_status->bind_result($status_name);
		$stmt_get_nonword_status->fetch();
		$stmt_get_nonword_status->close();
		
		$comment_type_ids = array();
		$comment_type_names = array();
		$is_lists = array();
		
		$stmt_get_comment_types = $con->prepare('select id, name, list from comment_type order by ordr');
		$stmt_get_comment_types->execute();
		$stmt_get_comment_types->bind_result($comment_type_id, $comment_type_name, $is_list);
		while($stmt_get_comment_types->fetch()) {
			$comment_type_ids[] = $comment_type_id;
			$comment_type_names[] = $comment_type_name;
			$is_lists[] = $is_list;
		}
		$stmt_get_comment_types->close();
		
		out(line(tag('h2', htmlspecialchars($word))));
		out(line(tabs(4) . '<div class="container">'));
		
		$num_comment_types = count($comment_type_ids);
		for($k = 0; $k < $num_comment_types; $k++) {
			$comment_type_id = $comment_type_ids[$k];
			$comment_type_name = $comment_type_names[$k];
			$is_list = $is_lists[$k];
			
			$comment_texts = array();
			
			$stmt_get_comment = $con->prepare('select text from comment where nonword_id = ? and comment_type_id = ? order by submit_time');
			$stmt_get_comment->bind_param('ii', $nonword_id, $comment_type_id);
			$stmt_get_comment->execute();
			$stmt_get_comment->bind_result($comment_text);
			while($stmt_get_comment->fetch()) {
				$comment_texts[] = $comment_text;
			}
			$stmt_get_comment->close();
			
			$num_comment_texts = count($comment_texts);
			if($num_comment_texts > 0) {
				$content = '';
				if($is_list) {
					$content .= $comment_texts[0];
					for($j = 1; $j < $num_comment_texts; $j++) {
						$content .= ', ' . $comment_texts[$j];
					}
					$content = line(tabs(7) . tag('p', $content));
				} else {
					if($comment_type_id == 3) {
						for($j = 0; $j < $num_comment_texts; $j++) {
							$content .= line(tabs(7) . tag('p', tag('em', $comment_texts[$j])));
						}
					} else {
						for($j = 0; $j < $num_comment_texts; $j++) {
							$content .= line(tabs(7) . tag('p', $comment_texts[$j]));
						}
					}
				}
				row($comment_type_name, $content);
			}
		}
		
		$con->close();
		
		row('Submitter', line(tabs(7) . tag('p', htmlspecialchars($user_name))));
		row('Added', line(tabs(7) . tag('p', $submit_time)));
		row('Status', line(tabs(7) . tag('p', $status_name)));
		
		out(line(tabs(4) . '</div>'));
	}
?>
