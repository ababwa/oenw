<?php
	include_once 'landing.php';
	include_once 'search.php';
	include_once 'nonword.php';
	include_once 'edit.php';
	include_once 'login.php';
	include_once 'submit.php';
	include_once 'about.php';
	
	const PAGE = 'page';
	
	const SEARCH = 'search';
	const NONWORD = 'nonword';
	const EDIT = 'edit';
	const LOGIN = 'login';
	const SUBMIT = 'submit';
	const ABOUT = 'about';
	
	const QUERY = 'query';
	const ID = 'id';
	const EMAIL = 'email';
	
	function content() {
		if(array_key_exists(PAGE, $_GET)) {
			$page = $_GET[PAGE];
			if($page == SEARCH) {
				if(array_key_exists(QUERY, $_GET)) {
					search($_GET[QUERY]);
				} else {
					landing();
				}
			} else if($page == NONWORD) {
				if(array_key_exists(ID, $_GET)) {
					nonword($_GET[ID]);
				} else {
					landing();
				}
			} else if($page == EDIT) {
				if(array_key_exists(ID, $_GET)) {
					edit($_GET[ID]);
				} else {
					landing();
				}
			} else if($page == LOGIN) {
				if(array_key_exists(EMAIL, $_GET)) {
					login_email($_GET[EMAIL]);
				} else {
					login();
				}
			} else if($page == SUBMIT) {
				submit();
			} else if($page == ABOUT) {
				about();
			}
		} else {
			landing();
		}
	}
	
	function page_name() {
		if(array_key_exists(PAGE, $_GET)) {
			echo $_GET[PAGE];
		} else {
			echo 'landing';
		}
	}
	
	function footer() {
		$con = get_db_connection();
		$stmt = $con->prepare('select last_modification, nonword_count from info');
		$stmt->execute();
		$stmt->bind_result($last_modification, $nonword_count);
		$stmt->fetch();
		$stmt->close();
		$con->close();
		echo '<p>Last modified ' . $last_modification . ' UTC<br>Contains ' . $nonword_count . ' non-words</p>' . "\n";
	}
?>
