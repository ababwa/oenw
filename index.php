<!-- Zane Jacobs
  -- Feb 3, 2018
  -- 
  -->

<?php include_once 'content.php' ?>
<!doctype html>
<html lang="en">
	<head>
		<title>The Online Encyclopedia of Non-Words</title>
		<meta charset="utf-8"/>
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/css/bootstrap.min.css" integrity="sha384-PsH8R72JQ3SOdhVi3uxftmaW6Vc51MKb0q5P2rRUpPvrszuE4W1povHYgTpBfshb" crossorigin="anonymous"/>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:100,300,400,500"/>
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Slab:300,400"/>
		<link rel="stylesheet" type="text/css" href="style.css"/>
	</head>
	<body>
		<div class="container">
			<header>
				<a href="."><h1>THE ONLINE ENCYCLOPEDIA<br>OF NON-WORDS</h1></a>
				<div class="line"></div>
				<h2>
					<div>Founded in 2017</div>
					<div>by Zane L. Jacobs</div>
				</h2>
			</header>
			<main class="<?php page_name(); ?>">
				<?php content(); ?>
			</main>
			<footer>
				<a href="?page=about">About</a>
				<script>
					if(false) {
						document.write('<a href="?page=submit">Submit</a>');
					} else {
						document.write('<a href="?page=login">Login</a>');
					}
				</script>
				<?php footer(); ?>
			</footer>
		</div>
		<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
		<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
	</body>
</html>
